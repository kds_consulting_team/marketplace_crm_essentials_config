project_name: "block-keboola-crm_essentials-config"

################ Constants ################

constant: CONFIG_PROJECT_NAME {
  value: "block-keboola-crm_essentials-config"
}

constant: CONNECTION {
  value: "keboola_block_crm_essentials"
}

constant: SCHEMA_NAME {
  value: "WORKSPACE_542685775"
}
